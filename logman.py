#!/usr/bin/env python3

import os, datetime

class logman():

    def __init__(self, logpath='logman.log'):
        #if not os.path.isfile(logpath):
        #    raise OSError('Arquivo de log nao encontrado: ' + logpath)
        self.logfile = open (logpath, 'a')


    # -------------------------------------
    # Write to a log file
    # -------------------------------------
    def writelog (self, msg, echo_screen=False):
        now = str (datetime.datetime.now())
        self.logfile.write (now + ': ' + msg + '\n' )
        if echo_screen:
            print (msg)

if __name__=='__main__':
    l=logman("teste.log")
    l.writelog ("TESTE 123")
