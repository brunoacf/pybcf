
import time
import random
import struct
import select
import socket

# usado pela funcao ping
def chk(data):
	x = sum(x << 8 if i % 2 else x for i, x in enumerate(data)) & 0xFFFFFFFF
	x = (x >> 16) + (x & 0xFFFF)
	x = (x >> 16) + (x & 0xFFFF)
	return struct.pack('<H', ~x & 0xFFFF)


# Dispara pings contra um determinado host (addr) 
# Ipv4.
def ping(addr, timeout=1, number=1, data=b''):
	with socket.socket(socket.AF_INET, socket.SOCK_RAW, socket.IPPROTO_ICMP) as conn:
		payload = struct.pack('!HH', random.randrange(0, 65536), number) + data

		conn.connect((addr, 80))
		conn.sendall(b'\x08\0' + chk(b'\x08\0\0\0' + payload) + payload)
		start = time.time()

		while select.select([conn], [], [], max(0, start + timeout - time.time()))[0]:
			data = conn.recv(65536)
			if len(data) < 20 or len(data) < struct.unpack_from('!xxH', data)[0]:
				continue
			if data[20:] == b'\0\0' + chk(b'\0\0\0\0' + payload) + payload:
				return time.time() - start




def tcpcon (host, port):

	print ("Tentando conexao com %s:%s ..."%(host, port))

	# Configura uma conexao
	cli = socket.socket (socket.AF_INET, socket.SOCK_STREAM)
	# Timeout de 10s.
	cli.settimeout(10)

	# Tenta a conexão
	try:
		cli.connect ( (host, port) )
	except socket.gaierror:
		print ("Erro: nome ou endereco invalido: %s"%(host))
		return False
	except ConnectionRefusedError:
		print ("Erro: conexao recusada")
		return False
	except socket.timeout:
		print ("Erro: time out")
		return False
	except OSError:
		print ("OSError")
		return False
	except:
		print ("Erro desconhecido")
		raise
		return False
	else:
		print ("OK, conexao aceita")
		cli.close()
		return True


#tcpcon(host, port)

if __name__ == '__main__':
	print(ping('127.0.0.1'))
