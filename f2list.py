
# ---------------------------------------
# Load a file into a list
# ---------------------------------------
def f2list (filename, strip=False, noblanks=False, commentchar=''):
    L = []
    try:
        f = open (filename, "r")
        while True:
            line = f.readline()
            # if eof
            if (line == ''):
                break

            # Strips spaces, newlines, tabs
            if strip==True:
                line = line.strip()
                # Ignore lines 'emptied' by the previous strip() function.
                if (line==''):
                    continue

            # Ignore empty lines
            if noblanks==True:
                if (line.strip()==''):
                    continue

            # Ignore comments (#)
            if commentchar!='':
                if (line.strip()[0]==commentchar):
                    continue

            L.append(line)

        f.close()
    except IOError:
        raise Exception ('f2list(): Error loading file: %s'%(filename))

    return L



#########################################
# Removes duplicated itens from a list
#########################################
def remove_dups(L):
    U = []
    for item in L:
        if item not in U:
            U.append(item)
    return U

