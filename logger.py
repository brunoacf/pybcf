import datetime


# -------------------------------------
# Class to log messages
# -------------------------------------
class logger:
	def __init__(self, logpath='messages.log'):
		self.logpath = logpath
		self.logfile = open (self.logpath, 'a')

	def write (self, msg, echoscr=False):
		now = str (datetime.datetime.now())
		self.logfile.write (now + ' ' + msg + '\n' )
		if echoscr:
			print (msg)

	def close (self):
		self.logfile.close()


if __name__ == "__main__":
	log = logger()
	log.write ("TESTE", True)
	log2 = logger ("testelog.log")
	log2.write ("TESTE123", True)


